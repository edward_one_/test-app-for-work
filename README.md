## Чтобы развернуть проект

### Клонируем проект
```html
git clone
```

### Устанавливаем зависимости
```html
composer install
```
## Настройте .env файл

### Выполнить накатку миграций и запуск наполнения тестовых данных
```html
php artisan migrate:fresh --seed
```

### Запуск локального сервера
```html
php artisan run serve
```

### Запуск планировщика
```html
php artisan schedule:work
```

### Запуск очередей
```html
php artisan queue:work
```

# API
## Для каждого запроса к API ОБЯЗАТЕЛЬНЫМ является заголовок
```html
Accept: application/json
```

### Метод для поиска по городу
```html
GET /api/zip-code

params: 
  title: required|string|min:2
```

### Метод для поиска по коду, где zipCode - код
```html
GET /api/zip-code/{zipCode}
```

### Метод для импорта файла, в ответ получите уникальный UUID, по которому можно будет узнать статус загрузки файла
```html
POST /api/imports/zip-code

params:
    import_csv: required|file|mimes:csv,txt
```

### Метод для получения статуса загрузки файла
```html
GET /api/imports/status/{uuid}
```

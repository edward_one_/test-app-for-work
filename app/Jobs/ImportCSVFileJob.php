<?php

namespace App\Jobs;

use App\Models\Import;
use App\Services\FileCSVImporter;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class ImportCSVFileJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $import = Import::where('status', Import::STATUS_IN_QUEUE)->first();
        if($import) {
            try {
                $import->update([
                    'status' => Import::STATUS_IN_WORK
                ]);

                FileCSVImporter::importFromCSV($import->file_path);

                $import->update([
                    'status' => Import::STATUS_IS_READY
                ]);
            } catch (\Exception $exception) {
                $import->update([
                    'exception' => $exception->getMessage(),
                    'status' => Import::STATUS_HAS_ERROR
                ]);
            }
        }
    }
}

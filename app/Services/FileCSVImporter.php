<?php

namespace App\Services;

use App\Models\City;
use App\Models\CityCountyInfo;
use App\Models\County;
use App\Models\State;
use App\Models\Timezone;

class FileCSVImporter
{
    public const CONFIG = [
            'timezone' => [
                'title'         => 'timezone'
            ],
            'county' => [
                'fips'          => 'countyFips',
                'title'         => 'countyName',
            ],
            'state' => [
                'short_title'   => 'stateId',
                'title'         => 'stateName',
            ]
        ];

    public static function importFromCSV(string $filePath)
    {
        $data = static::readCSV($filePath, ['delimiter' => ',']);

        $repository = [
            'timezone'          => [],
            'county'            => [],
            'state'             => [],
        ];

        $modelsFromDB = [
            'timezone'          => [],
            'county'            => [],
            'state'             => [],
        ];

        $dataLength = count($data);

        if($dataLength > 1) {
            for ($i = 1; $i < $dataLength; $i++) {
                if(is_array($data[$i])) {
                    // Тут вызываем trim для каждой ячейки из csv
                    $rawData = static::prepareRaw($data[$i]);

                    // Чтобы не делать на каждую строку из csv файла много запросов в БД, можно определить уникальные
                    // значения и запишем только их. (Проводил эксперемент, это уменьшает кол-во запросов к БД примерно в
                    // 700 раз)
                    foreach (static::CONFIG as $repositoryName => $settings) {
                        $query = [];
                        foreach ($settings as $field => $key) {
                            $query[$field] = $rawData[$key];
                        }
                        $valueFromRepository = static::findByFields($query, $repository[$repositoryName]);

                        $alreadyExistsInRepository = !is_null($valueFromRepository);
                        if(!$alreadyExistsInRepository) {
                            $repository[$repositoryName][] = $query;
                        }
                    }
                }
            }
        }

        foreach ($repository as $repositoryName => $values) {
            switch ($repositoryName){
                case 'timezone':
                    foreach ($values as $value) {
                        $modelsFromDB[$repositoryName][] = Timezone::updateOrCreate($value)->toArray();
                    }
                    break;
                case 'county':
                    foreach ($values as $value) {
                        $modelsFromDB[$repositoryName][] = County::updateOrCreate($value)->toArray();
                    }
                    break;
                case 'state':
                    foreach ($values as $value) {
                        $modelsFromDB[$repositoryName][] = State::updateOrCreate($value)->toArray();
                    }
                    break;
            }
        }

        if($dataLength > 1) {
            for($i = 1; $i < $dataLength; $i++) {
                if (is_array($data[$i])) {
                    $rawData = static::prepareRaw($data[$i]);

                    $timezoneModel = static::getModelFromDBByName('timezone', $rawData, $modelsFromDB);
                    $countyModel = static::getModelFromDBByName('county', $rawData, $modelsFromDB);
                    $stateModel = static::getModelFromDBByName('state', $rawData, $modelsFromDB);

                    $cityModel = City::updateOrCreate(
                        ['zip_code' => $rawData['zipCode']],
                        [
                            'zip_code' => $rawData['zipCode'],
                            'latitude' => $rawData['latitude'],
                            'longitude' => $rawData['longitude'],
                            'title' => $rawData['cityTitle'],
                            'zcta' => static::tryToConvertToBoolean($rawData['zcta']),
                            'parent_zcta' => $rawData['parentZcta'] ?? null,
                            'population' => (int) ($rawData['population'] ?? 0),
                            'density' => (float) ($rawData['density'] ?? 0),
                            'imprecise' => static::tryToConvertToBoolean($rawData['imprecise']),
                            'military' => static::tryToConvertToBoolean($rawData['military']),

                            'state_id' => $stateModel['id'],
                            'county_id' => $countyModel['id'],
                            'timezone_id' => $timezoneModel['id'],
                        ]
                    );

                    $countyWeights = json_decode($rawData['countyWeights']);

                    foreach ($countyWeights as $key => $value) {
                        $query = ['fips' => $key];
                        $countyEntity = static::findByFields($query, $modelsFromDB['county']);
                        if($countyEntity) {
                            CityCountyInfo::firstOrCreate([
                                'city_id' => $cityModel->id,
                                'county_id' => $countyEntity['id'],
                                'weight' => $value,
                            ]);
                        }
                    }
                }
            }
        }
    }

    public static function readCSV($csvFile, $array)
    {
        $file_handle = fopen(public_path($csvFile), 'r');
        $line_of_text = [];
        while (!feof($file_handle)) {
            $line_of_text[] = fgetcsv($file_handle, 0, $array['delimiter']);
        }
        fclose($file_handle);
        return $line_of_text;
    }

    public static function tryToConvertToBoolean($string = ''): bool
    {
        $lowerText = strtolower($string);

        return ($lowerText !== '') && $lowerText === 'true';
    }

    public static function prepareColumnValue(string $raw): string
    {
        return trim($raw);
    }

    public static function prepareRaw(array $raw): array
    {
        $result = [];

        $result['zipCode']            = static::prepareColumnValue($raw[0]);
        $result['latitude']           = static::prepareColumnValue($raw[1]);
        $result['longitude']          = static::prepareColumnValue($raw[2]);
        $result['cityTitle']          = static::prepareColumnValue($raw[3]);
        $result['stateId']            = static::prepareColumnValue($raw[4]);
        $result['stateName']          = static::prepareColumnValue($raw[5]);
        $result['zcta']               = static::prepareColumnValue($raw[6]);
        $result['parentZcta']         = static::prepareColumnValue($raw[7]);
        $result['population']         = static::prepareColumnValue($raw[8]);
        $result['density']            = static::prepareColumnValue($raw[9]);
        $result['countyFips']         = static::prepareColumnValue($raw[10]);
        $result['countyName']         = static::prepareColumnValue($raw[11]);
        $result['countyWeights']      = static::prepareColumnValue($raw[12]);
        $result['countyNamesAll']     = static::prepareColumnValue($raw[13]);
        $result['countyFipsAll']      = static::prepareColumnValue($raw[14]);
        $result['imprecise']          = static::prepareColumnValue($raw[15]);
        $result['military']           = static::prepareColumnValue($raw[16]);
        $result['timezone']           = static::prepareColumnValue($raw[17]);

        return $result;
    }

    public static function findByFields($params, $array)
    {
        foreach ($array as $arrPosition => $arrayContent) {
            $allDone = true;
            foreach ($params as $field => $value) {
                if($arrayContent[$field] != $value) {
                    $allDone = false;
                }
            }

            if($allDone) {
                return $arrayContent;
            }
        }
        return null;
    }

    public static function getModelFromDBByName(string $name, array $rawData, array $modelsFromDB)
    {
        $query = [];
        foreach (static::CONFIG[$name] as $field => $key) {
            $query[$field] = $rawData[$key];
        }
        return static::findByFields($query, $modelsFromDB[$name]);
    }
}

<?php

namespace App\Services;

use Illuminate\Http\Request;

class FileUploaderService
{
    private Request $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function uploadFileByKey(string $key = 'key')
    {
        return $this->request->file($key)->store('public/imports');
    }
}

<?php

namespace App\Filters;

class CityFilter extends QueryFilter
{
    public function title(string $title)
    {
        $this->builder->where('title', 'like', "%$title%");
    }
}

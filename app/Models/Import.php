<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Import extends Model
{
    use HasFactory;

    public const STATUS_IN_QUEUE = 0;
    public const STATUS_IN_WORK = 1;
    public const STATUS_IS_READY = 2;
    public const STATUS_HAS_ERROR = 3;

    protected $guarded = ['id'];
}

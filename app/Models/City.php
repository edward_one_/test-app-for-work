<?php

namespace App\Models;

use App\Traits\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class City extends Model
{
    use HasFactory, Filterable;

    protected $guarded = ['id'];

    protected $casts = [
        'zcta'      => 'boolean',
        'imprecise' => 'boolean',
        'military'  => 'boolean',
    ];

    public function timezone(): BelongsTo
    {
        return $this->belongsTo(Timezone::class);
    }

    public function county(): BelongsTo
    {
        return $this->belongsTo(County::class);
    }

    public function state(): BelongsTo
    {
        return $this->belongsTo(State::class);
    }

    public function city_county_infos(): HasMany
    {
        return $this->hasMany(CityCountyInfo::class);
    }
}

<?php

namespace App\Http\Controllers;

use App\Filters\CityFilter;
use App\Http\Requests\CityImportRequest;
use App\Http\Requests\CityIndexRequest;
use App\Http\Resources\CityResource;
use App\Models\City;
use App\Models\Import;
use App\Services\FileCSVReader;
use App\Services\FileUploaderService;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CityController extends Controller
{
    public function index(CityIndexRequest $request, CityFilter $filter)
    {
        $cities = City::filter($filter)
            ->with(['timezone', 'state', 'county', 'city_county_infos.county'])
            ->limit(500)
            ->get();

        if(!$cities->isEmpty()) {
            return CityResource::collection($cities);
        }

        abort(404);
    }

    public function show(Request $request, $zipCode): CityResource
    {
        return new CityResource(City::where('zip_code', $zipCode)
            ->with(['timezone', 'state', 'county', 'city_county_infos.county'])
            ->firstOrFail()
        );
    }

    public function import(CityImportRequest $request,
        FileUploaderService $fileUploaderService)
    {
        $uuid = (string) Str::uuid();
        $csvFile = $fileUploaderService->uploadFileByKey('import_csv');
        $filePath = str_replace('public', 'storage', $csvFile);

        Import::create([
            'uuid' => $uuid,
            'file_path' => $filePath,
            'status' => Import::STATUS_IN_QUEUE
        ]);

        return response()->json(['data' => $uuid]);
    }

    public function getStatus(Request $request, string $uuid)
    {
        $statuses = [
            Import::STATUS_IN_QUEUE => 'In queue',
            Import::STATUS_IN_WORK  => 'In work',
            Import::STATUS_IS_READY => 'Is ready',
            Import::STATUS_HAS_ERROR => 'Has error',
        ];

        $import = Import::where('uuid', $uuid)->firstOrFail();

        $response = [
            'status' => $statuses[$import->status]
        ];

        if($import->status === Import::STATUS_HAS_ERROR && $import->exception) {
            $response['error'] = $import->exception;
        }
        return response()->json($response);
    }
}

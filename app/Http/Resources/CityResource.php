<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'title'             => $this->title,
            'zip'               => $this->zip_code,
            'lat'               => $this->latitude,
            'lng'               => $this->longitude,
            'state_id'          => optional($this->state)->short_title,
            'state_name'        => optional($this->state)->title,
            'zcta'              => $this->zcta,
            'parent_zcta'       => $this->parent_zcta,
            'population'        => $this->population,
            'density'           => $this->density,
            'county_fips'       => optional($this->county)->fips,
            'county_name'       => optional($this->county)->title,
            'county_names_all'  => collect(optional($this->city_county_infos)->all())->pluck('county.title')->implode('|'),
            'county_fips_all'   => collect(optional($this->city_county_infos)->all())->pluck('weight')->implode('|'),
            'county_weights'    => collect(optional($this->city_county_infos)->all())
                ->map(function ($data) {
                    return [
                        (string)$data->county_id => (string)$data->weight
                    ];
                }),
            'imprecise'         => $this->imprecise,
            'military'          => $this->military,
            'timezone'          => optional($this->timezone)->title,
        ];
    }
}

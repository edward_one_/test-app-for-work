<?php

namespace Database\Factories;

use App\Models\City;
use Illuminate\Database\Eloquent\Factories\Factory;

class CityFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = City::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->city,
            'zip_code' => $this->faker->unique()->randomNumber(5),
            'latitude' => $this->faker->randomFloat(5, 50, 80),
            'longitude' => $this->faker->randomFloat(5, 50, 80),
            'timezone_id' => $this->faker->randomFloat(0, 1, 50),
            'state_id' => $this->faker->randomFloat(0, 1, 50),
            'county_id' => $this->faker->randomFloat(0, 1, 50),
            'zcta' => $this->faker->randomElement([true, false]),
            'population' => $this->faker->randomNumber(5),
            'density' => $this->faker->randomFloat(1, 0, 100),
            'imprecise' => $this->faker->randomElement([true, false]),
            'military' => $this->faker->randomElement([true, false]),
        ];
    }
}

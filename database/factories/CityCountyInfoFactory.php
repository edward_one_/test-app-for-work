<?php

namespace Database\Factories;

use App\Models\CityCountyInfo;
use Illuminate\Database\Eloquent\Factories\Factory;

class CityCountyInfoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CityCountyInfo::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'city_id'   => $this->faker->unique()->randomFloat(0, 1, 100),
            'county_id' => $this->faker->unique()->randomFloat(0, 1, 100),
            'weight'    => $this->faker->randomFloat(2, 1, 100),
        ];
    }
}

<?php

namespace Database\Seeders;

use App\Models\CityCountyInfo;
use Illuminate\Database\Seeder;

class CityCountyInfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CityCountyInfo::factory()->count(50)->create();
    }
}

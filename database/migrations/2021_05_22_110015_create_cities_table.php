<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('zip_code');
            $table->decimal('latitude', 10, 8);
            $table->decimal('longitude', 11, 8);
            $table->foreignIdFor(\App\Models\State::class);
            $table->boolean('zcta')->default(false);
            $table->string('parent_zcta')->nullable();
            $table->integer('population')->default(0);
            $table->float('density')->default(0);
            $table->foreignIdFor(\App\Models\County::class);
            $table->boolean('imprecise')->default(false);
            $table->boolean('military')->default(false);
            $table->foreignIdFor(\App\Models\Timezone::class);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}

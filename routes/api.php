<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('api')->group(function () {

    Route::get('/zip-code',                     'App\Http\Controllers\CityController@index');
    Route::get('/zip-code/{zipCode}',           'App\Http\Controllers\CityController@show');
    Route::post('/imports/zip-code',            'App\Http\Controllers\CityController@import');

    Route::get('/imports/status/{uuid}',        'App\Http\Controllers\CityController@getStatus');

});
